<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ユーザー編集</title>
			<c:if test="${ loginUser.branch_id == '1' && loginUser.department_id == 1 || loginUser.department_id == 2 }">
				<div class="form-area">
					<span class="link">
						<a href="./">ホーム</a>
				</span>
				<span class="link">
					<c:if test="${ (loginUser.department_id == 1) && loginUser.branch_id == 1}">
						<a href="management">ユーザー管理</a>
					</c:if>
				</span>
				<span>ユーザー編集</span>
				<span class="logout" style="float: right;">
					<a href="logout">ログアウト</a>
				</span>
				<span class="loginUser" style="float: right;"><small>LoginUser:<c:out value="${loginUser.name}"/></small></span>
				</div>
			</c:if>

<script type="text/javascript">
<!--
function disp(){

	// 「OK」時の処理開始 ＋ 確認ダイアログの表示
	if(window.confirm('登録しますか？')){
		return true;

	}
	// 「OK」時の処理終了

	// 「キャンセル」時の処理開始
	else{

		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false;

	}
	// 「キャンセル」時の処理終了

}

function undisabled()
{
    document.getElementById( "department_id" ).disabled = false ;
    return true;
    document.getElementById( "branch_id" ).disabled = false ;
    return true;
}
//-->
</script>
</head>
<body>

	<div class="main-contents">
				<c:if test="${ not empty errorMessages}">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errormsg">
							<li><c:out value="${errormsg}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
		<form action="edit" method="post">

				<input type="hidden" name="id" value="${edit.id }">
				<table class="table">
					<tr>
						<th>ログインID </th>
						<td>
							<input type="text" name="account" id="account" value="${edit.account}">
						</td>
					</tr>
					<tr>
						<th>パスワード</th>
						<td>
							<input type="password" name="password" id="password" placeholder="パスワードを入力してください" >
						</td>
					</tr>
					<tr>
						<th>パスワード（確認用）</th>
						<td>
							<input type="password" name="pass_ck" id="pass_ck" placeholder="パスワード（確認用）を入力してください" >
						</td>
					</tr>
					<tr>
						<th>ユーザー名</th>
						<td>
						<input type="text" name="name" value="${edit.name }" id="name" placeholder="ユーザー名を入力してください" >
						</td>
					</tr>
					<tr>
						<th>部署・役職</th>
						<td>
						<select name ="department_id" id="department">
						<c:forEach items="${department}" var="department">
						<c:choose>
							<c:when test="${ loginUser.id == edit.id }">
								<c:choose>
								<c:when  test="${ department.id == edit.department_id }">
									<option value="${ department.id }" selected>${ department.name }</option>
								</c:when>
								<c:otherwise>
									<option value="${ department.id }" disabled="disabled">${ department.name }</option>
								</c:otherwise>
								</c:choose>
							</c:when>
							<c:otherwise>
								<c:choose>
								<c:when  test="${ department.id == edit.department_id }">
									<option value="${ department.id }" selected>${ department.name }</option>
								</c:when>
								<c:otherwise>
									<option value="${ department.id }">${ department.name }</option>
								</c:otherwise>
								</c:choose>
							</c:otherwise>
						</c:choose>
						</c:forEach>
						</select>
						</td>
					</tr>
					<tr>
						<th>支社・支店</th>
						<td>
						<select name="branch_id" id="branch">
							<c:forEach items="${branch}" var="branch">
							<c:choose>
								<c:when test="${ loginUser.id == edit.id }">
									<c:choose>
										<c:when  test="${ branch.id == edit.branch_id }">
											<option value="${branch.id}" selected>${branch.name}</option>
										</c:when>
										<c:otherwise>
											<option value="${branch.id}" disabled="disabled">${branch.name}</option>
										</c:otherwise>
									</c:choose>
								</c:when>
								<c:otherwise>
									<c:choose>
										<c:when  test="${ branch.id == edit.branch_id }">
											<option value="${branch.id}" selected>${branch.name}</option>
										</c:when>
										<c:otherwise>
											<option value="${branch.id}">${branch.name}</option>
										</c:otherwise>
									</c:choose>
								</c:otherwise>
							</c:choose>
							</c:forEach>
						</select>
						</td>
					</tr>
				</table>
		<div class="management_submit">
		<input type="submit" value="登録" text-align="center" onClick="return disp()" onclick="javascript:unDisabled();"/> <br />
		</div>
		</form>
	</div>
	<div class="copyright">Copyright(c)OsugaKensuke</div>
</body>
</html>