<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<script type="text/javascript" src="./js/jQuery.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>掲示板</title>
<script type="text/javascript">

<!--
function disp(){

	// 「OK」時の処理開始 ＋ 確認ダイアログの表示
	if(window.confirm('削除しますか？')){

		return true;

	}
	// 「OK」時の処理終了

	// 「キャンセル」時の処理開始
	else{

		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false;

	}
	// 「キャンセル」時の処理終了

}
//-->
$("#bord").css("color","#d9534f");
</script>
		<c:if test="${ not empty loginUser }">
			<div class="form-area">
			<span class="link">
					<a href="./">ホーム</a>
			</span>
			<span class="link">
					<a href="newMessage">投稿</a>
			</span>
			<c:if test="${ (loginUser.department_id == 1) && loginUser.branch_id == 1}">
				<span class="link">
						<a href="management">ユーザー管理</a>
				</span>
			</c:if>
			<span id="bord">掲示板</span>
			<span class="logout" style="float: right;">
				<a href="logout">ログアウト</a>
			</span>
			<span class="loginUser" style="float: right;"><small>LoginUser:<c:out value="${loginUser.name}"/></small></span>
			</div>
		</c:if>
</head>
<body>
	<div class="main-contents">

			<c:if test="${ not empty errorMessages}">
			<div class="errorMessages">
			<ul>
			<c:forEach items="${errorMessages}" var="errormsg">
				<li><c:out value="${errormsg}" />
			</c:forEach>
			</ul>
			</div>
            <c:remove var="errorMessages" scope="session"/>
			</c:if>

		<c:if test="${ isShowMessageForm }">
		<div class="conditions">
		<form action="./">
			<table>
				<tr>
					<th><label for="start">投稿日（開始日）:</label></th>
					<td>
					<input type="date" id="start" name="start" value="${ start }" min="2015-01-01 00:00:00" max="2019-12-31 00:00:00">
       				</td>
       			</tr>
       			<tr>
       				<th><label for="end">投稿日（終了日）:</label></th>
	   				<td>
	   				<input type="date" id="end" name="end" value="${ end }" min="2015-01-01 00:00:00" max="2019-12-31 00:00:00">
       				</td>
       			</tr>
       			<tr>
       				<th><label for="category" placeholder="カテゴリーを入力してください" >カテゴリー:</label></th>
	   				<td>
					<input type="text" name="param" value="${ num }">
       				</td>
       			</tr>
       		</table>
       		<div class="input_submit">
       			<input type="submit" value="絞込み">
       		</div>
       </form>
		</div>
		<c:forEach items="${messages}" var="message">
			<div class="messages">
				<div class="message">
					<div class="message_title">
					<small>件名</small>
						<span class="subject"><c:out value="${message.subject}" /></span>
							<span class="input_name">投稿者:<c:out value="${message.name}" /></span>
						<br />
						<div class="message_text">
							<span class="text">
								<c:out value="${message.text}" />
							</span><br><br><br>
								<span class="input_category">カテゴリー:<c:out value="${message.category}" /></span>
							<small>
								<span class="input_date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy-MM-dd HH:mm:ss" /></span>
							</small>
						<br />
						<c:if test="${ loginUser.id == message.user_id }">
						<form action="messagedelete" method="post">
						<input type="hidden" name="message_id" value="${ message.id }">
							<div class="input_submit">
								<input type="submit" class="btn" value="削除" onClick="return disp()">
							</div>
						</form>
						</c:if>
						</div>
					</div>
 					<c:forEach items="${comments}" var="comment">

						<c:if test="${ message.id == comment.message_id }">
							<div class="comment_title">
								<span class="name"><small>投稿者:<c:out value="${comment.name}" /></small></span> <br />
								<div class="comment_text">
									<span class="text">
									<c:out value="${comment.text}" />
									</span>
									<br><br><br>
										<small>
					            			<span class="input_date"><fmt:formatDate value="${message.createdDate}" pattern="yyyy-MM-dd HH:mm:ss" /></span>
					            		</small><br>
									<%--  コメント削除用 --%>
									<c:if test="${ loginUser.id == comment.user_id }">
										<form action="delete" method="post">
											<input type="hidden" name="comment_id" value="${ comment.id }">
											<div class="input_submit">
											<input type="submit" class="delete_btn" value="削除" onClick="return disp()">
											</div>
									</form>
									</c:if>
								</div>
							</div>
						</c:if>
					</c:forEach>
					<form action="comment" method="post">
						<br />
						<input type="hidden" name="message_id" value="${message.id}">
						<textarea name="comment" wrap="soft" class="comment-box" rows="7" placeholder="コメントを入力してください（500文字まで）" ></textarea>
						<div class="input_submit">
							<input type="submit" id="comment" value="コメント">
						</div>
					</form>
					</div>
				</div>
				</c:forEach>
		</c:if>
	</div>
	<div class="copyright">Copyright(c)OsugaKensuke</div>
</body>
</html>
