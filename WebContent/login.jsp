<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ログイン</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <div class="login">
            <form action="login" method="post">
                <label for="account">ログインID</label><br/>
                <input name="account" id="login_account" value="${ account }" placeholder="ログインIDを入力してください" /> <br />

                <label for="password">パスワード</label><br/>
                <input name="password" type="password" id="login_password" placeholder="パスワードを入力してください" /> <br />
                <div class="management_submit">
                <input type="submit" value="ログイン" /> <br />
                </div>
            </form>
            </div>
        </div>
        <div class="copyright"> Copyright(c)OsugaKensuke</div>
</body>
</html>