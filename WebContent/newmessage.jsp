<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>新規投稿</title>
			<div class="form-area">
				<span class="link">
					<a href="./">ホーム</a>
				</span>
				<span class="link">
					<c:if test="${ (loginUser.department_id == 1 || loginUser.department_id == 2) && loginUser.branch_id == 1}">
						<a href="management">ユーザー管理</a>
					</c:if>
				</span>
				<span>新規投稿</span>
				<span class="logout" style="float: right;">
					<a href="logout">ログアウト</a>
				</span>
				<span class="loginUser" style="float: right;"><small>LoginUser:<c:out value="${loginUser.name}"/></small></span>
			</div>
			<br/>
</head>
<body>
	<div class="main-contents">
			<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="newMessage" method="post">
		<table class="table">
		<tr>
			<th>件名</th>
			<td>
			<input name="subject" id="subject" value="${ subject }" placeholder="件名を入力してください(件名は30文字以下とします)" /><br />
			</td>
		</tr>
		<tr>
			<th>カテゴリー</th>
			<td>
		 		<input name="category" id="category" value="${ category }" placeholder="カテゴリーを入力してください(カテゴリー名は10文字以下とします)" /> <br />
			</td>
		</tr>
		<tr>
			<th>メッセージ</th>
			<td>
			<textarea name="text" cols="100" rows="7" wrap="soft" class="newmessage-box" placeholder="メッセージを入力してください(本文は1000文字以下とします)" >${ message }</textarea><br />
		    <br />
		    </td>
		</tr>
		</table>
		<div class="management_submit">
		<input type="submit" value="投稿" /> <br />
		</div>
		</form>
	</div>
	<div class="copyright">Copyright(c)OsugaKensuke</div>
</body>
</html>