<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="./css/style.css" rel="stylesheet" type="text/css">
    <title>ユーザー登録</title>
		<c:if test="${ loginUser.branch_id == '1' && (loginUser.department_id == 1 || loginUser.department_id == 2) }">
			<div class="form-area">
				<span class="link">
					<a href="./">ホーム</a>
				</span>
				<span class="link">
					<c:if test="${ (loginUser.department_id == 1) && loginUser.branch_id == 1}">
						<a href="management">ユーザー管理</a>
					</c:if>
				</span>
				<span>ユーザー登録</span>
				<span class="logout" style="float: right;">
					<a href="logout">ログアウト</a>
				</span>
				<span class="loginUser" style="float: right;"><small>LoginUser:<c:out value="${loginUser.name}"/></small></span>
			</div>
		</c:if>
    </head>
    <body>
        <div class="main-contents">
                        <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" /></li>
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
            <form action="signup" method="post">
            	<table class="table">
					<tr>
						<th>ログインID</th>
						<td>
                			<input name="accountname" value="${ accountname }" id="account" placeholder="ログインIDを入力してください(半角英数字で6文字以上20文字以下とします)"/> <br />
                		</td>
                	</tr>
                	<tr>
                		<th>パスワード</th>
                		<td>
                			<input type="password" name="password" id="password" placeholder="パスワードを入力してください(記号を含む半角文字で6文字以上20文字以下とします)" /><br />
                		</td>
                	</tr>
                	<tr>
                		<th>パスワード（確認用）</th>
                		<td>
                			<input type="password" name="pass_ck" id="pass_ck" placeholder="パスワード（確認用）を入力してください"/> <br />
                		</td>
                	</tr>
                	<tr>
                		<th>ユーザー名</th>
                		<td>
                		    <input name="name" id="name" value="${ name }"  placeholder="ユーザー名を入力してください(名称は10文字以下とします)"/> <br />
                		</td>
                	</tr>
                	<tr>
                		<th>部署・役職</th>
                		<td>
							<select name="department" id="department">
								<c:forEach items="${department}" var="department">
								<c:choose>
								<c:when test="${ department.id == department_id}">
									<option value="${department.id}" selected>${department.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${department.id}">${department.name}</option>
								</c:otherwise>
								</c:choose>
								</c:forEach>
							</select>
						</td>
					</tr>
					<tr>
						<th>支社・支店</th>
						<td>
							<select name="branch" id="branch">
								<c:forEach items="${branch}" var="branch">
								<c:choose>
								<c:when test="${ branch.id == branch_id }">
									<option value="${branch.id}" selected>${branch.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${branch.id}">${branch.name}</option>
								</c:otherwise>
								</c:choose>
								</c:forEach>
							</select>
						</td>
					</tr>
				</table>
				<div class="management_submit">
                	<input type="submit" value="登録" />
                </div>
            </form>
        </div>
        <div class="copyright">Copyright(c)OsugaKensuke</div>
    </body>
</html>