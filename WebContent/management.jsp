<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ユーザー管理</title>
		<c:if test="${ loginUser.branch_id == '1' && (loginUser.department_id == 1) }">
			<div class="form-area">
				<span class="link">
					<a href="./">ホーム</a>
				</span>
				<span class="link">
					<a href="newMessage">投稿</a>
				</span>
				<span class="link">
					<a href="signup">ユーザー登録</a>
				</span>
				<span>ユーザー管理</span>
				<span class="logout" style="float: right;">
					<a href="logout">ログアウト</a>
				</span>
				<span class="loginUser" style="float: right;"><small>LoginUser:<c:out value="${loginUser.name}"/></small></span>
			</div>
		</c:if>
<script type="text/javascript">
<!--
function again(){

	// 「OK」時の処理開始 ＋ 確認ダイアログの表示
	if(window.confirm('アカウントを復活しますか？')){
		return true;

	}
	// 「OK」時の処理終了

	// 「キャンセル」時の処理開始
	else{

		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false;

	}
	// 「キャンセル」時の処理終了

}
function stop(){

	// 「OK」時の処理開始 ＋ 確認ダイアログの表示
	if(window.confirm('アカウントを停止しますか？')){
		return true;

	}
	// 「OK」時の処理終了

	// 「キャンセル」時の処理開始
	else{

		window.alert('キャンセルされました'); // 警告ダイアログを表示
		return false;

	}
	// 「キャンセル」時の処理終了

}
//-->
</script>
</head>
<body>

	<div class="main-contents">
		<c:if test="${ not empty errorMessages}">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="errormsg">
					<li><c:out value="${errormsg}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>
	<table class="table">
				<tr>
					<th><font>ログインID</font></th>
					<th><font>ユーザー名</font></th>
					<th><font>部署・役職</font></th>
					<th><font>支社・支店</font></th>
					<th><font>復活・停止</font></th>
					<th><font>ユーザー編集</font></th>
				</tr>
		<c:forEach items="${userlist}" var="userlist">

				<tr>
					<td><font><c:out value="${userlist.account}" /></font></td>
					<td><font><c:out value="${userlist.name}" /></font></td>
					<td><font><c:out value="${userlist.department}" /></font></td>
					<td><font><c:out value="${userlist.branch}" /></font></td>
					<td>
					<form action="management" method="post">
					<c:choose>
					<c:when test="${ loginUser.id == userlist.id }">
							<c:out value="ログイン中"/>
					</c:when>
					<c:when test="${ loginUser.id != userlist.id }">
						<c:choose>
						<c:when test="${userlist.isstoped == 1 }" ><%--
							<input type="submit" name="management" value="復活" onClick="return again()">
							<input type="hidden" name="id" value="${ userlist.id }"> --%>
							<button name="management" value="0" onClick="return again()">復活</button>
							<input type="hidden" name="id" value="${ userlist.id }">
						</c:when>
						<c:when test="${userlist.isstoped == 0 }" >
							<button name="management" value="1" onClick="return stop()">停止</button>
							<input type="hidden" name="id" value="${ userlist.id }">
						</c:when>
						</c:choose>
					</c:when>
					</c:choose>
					</form>
					</td>
					<td>
					<form action="edit">
					<input type="submit" value="編集">
					<input type="hidden" name="id" value="${ userlist.id }">
					</form>
					</td>
				</tr>
		</c:forEach>
		</table>
		<br>
	</div>
	<div class="copyright">Copyright(c)OsugaKensuke</div>
</body>
</html>