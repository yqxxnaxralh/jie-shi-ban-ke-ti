/**
 *
 */
package bulletin_board.dao;

import static bulletin_board.util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bulletin_board.beans.User;
import bulletin_board.exception.SQLRuntimeException;

/**
 * @author osuga.kensuke
 *
 */
public class UserDao {

	public User getUser(Connection connection, String account, String encPassword) {
		// TODO 自動生成されたメソッド・スタブ
		PreparedStatement ps = null;
		try {

			String sql = "SELECT * FROM users WHERE account = ? AND password = ? ";

			ps = connection.prepareStatement(sql);
			ps.setString(1, account);
			ps.setString(2, encPassword);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String password = rs.getString("password");
				String account = rs.getString("account");
				String name = rs.getString("name");
				int department_id = rs.getInt("department_id");
				int branch_id = rs.getInt("branch_id");
				int isstoped = rs.getInt("isstoped");
				Timestamp created_date = rs.getTimestamp("created_date");
				Timestamp updated_date = rs.getTimestamp("updated_date");

				User user = new User();
				user.setId(id);
				user.setPassword(password);
				user.setAccount(account);
				user.setName(name);
				user.setDepartment_id(department_id);
				user.setBranch_id(branch_id);
				user.setIsstoped(isstoped);
				user.setCreatedDate(created_date);
				user.setUpdatedDate(updated_date);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	//	ユーザー登録用
	public void insert(Connection connection, User user) {
		// TODO 自動生成されたメソッド・スタブ

		PreparedStatement ps = null;

		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append(" password");
			sql.append(", account");
			sql.append(", name");
			sql.append(", department_id");
			sql.append(", branch_id");
			sql.append(", isstoped");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append(" ?"); // password
			sql.append(", ?"); // account
			sql.append(", ?"); // name
			sql.append(", ?"); // department_id
			sql.append(", ?"); // branch_id
			sql.append(", '0'"); // isstoped
			sql.append(", CURRENT_TIMESTAMP"); // created_date
			sql.append(", CURRENT_TIMESTAMP"); // updated_date
			sql.append(")");
			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getPassword());
			ps.setString(2, user.getAccount());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getDepartment_id());
			ps.setInt(5, user.getBranch_id());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<String> getAccount(Connection connection) {
		// TODO 自動生成されたメソッド・スタブ
		PreparedStatement ps = null;
		List<String> accountList = new ArrayList<String>();
		try {

			String sql = "SELECT account FROM users";

			ps = connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				String account = rs.getString("account");
				accountList.add(account);
			}
			return accountList;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<User> Department(Connection connection) {
		// TODO 自動生成されたメソッド・スタブ
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("id, ");
			sql.append("name ");
			sql.append("FROM departments ");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<User> ret = SinupList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<User> Branch(Connection connection) {
		// TODO 自動生成されたメソッド・スタブ
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("id, ");
			sql.append("name ");
			sql.append("FROM branches ");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<User> ret = SinupList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<User> SinupList(ResultSet rs)
			throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");

				User edit = new User();
				edit.setId(id);
				edit.setName(name);

				ret.add(edit);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
