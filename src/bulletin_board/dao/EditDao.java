package bulletin_board.dao;

import static bulletin_board.util.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import bulletin_board.beans.Branch;
import bulletin_board.beans.Department;
import bulletin_board.beans.User;
import bulletin_board.exception.SQLRuntimeException;

public class EditDao {

	public User getEditDao(Connection connection, int id) {
		// TODO 自動生成されたメソッド・スタブ
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("users.id, ");
			sql.append("users.password,");
			sql.append("users.account,");
			sql.append("users.name, ");
			sql.append("users.department_id, ");
			sql.append("users.branch_id, ");
			sql.append("departments.name as department, ");
			sql.append("branches.name as branch, ");
			sql.append("users.isstoped ");
			sql.append("FROM users ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department_id = departments.id ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("WHERE users.id = ? ");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			List<User> userList = toEditList(rs);
			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	private List<User> toEditList(ResultSet rs)
			throws SQLException {
		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String password = rs.getString("password");
				String account = rs.getString("account");
				String name = rs.getString("name");
				int department_id = rs.getInt("department_id");
				int branch_id = rs.getInt("branch_id");
				String department = rs.getString("department");
				String branch = rs.getString("branch");
				int isstoped = rs.getInt("isstoped");

				User edit = new User();
				edit.setId(id);
				edit.setPassword(password);
				edit.setPass_ck(password);
				edit.setAccount(account);
				edit.setName(name);
				edit.setDepartment_id(department_id);
				edit.setBranch_id(branch_id);
				edit.setDepartment(department);
				edit.setBranch(branch);
				edit.setIsstoped(isstoped);

				ret.add(edit);

			}
		} finally {
			close(rs);
		}
		return ret;
	}

	public User Update(Connection connection, User edit, User user) {
		// TODO 自動生成されたメソッド・スタブ
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users ");
			sql.append("SET ");
			sql.append("id = ?,");
			if(edit.getPassword() == null) {
				sql.append("account = ?,");
				sql.append("name = ?, ");
				sql.append("department_id = ?, ");
				sql.append("branch_id = ?,");
				sql.append("updated_date = CURRENT_TIMESTAMP ");
				sql.append("WHERE ");
				sql.append("id = ? ");
			}else {
				sql.append("password = ?,");
				sql.append("account = ?,");
				sql.append("name = ?, ");
				sql.append("department_id = ?, ");
				sql.append("branch_id = ?,");
				sql.append("updated_date = CURRENT_TIMESTAMP ");
				sql.append("WHERE ");
				sql.append("id = ? ");
			}


			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, edit.getId());
			if(edit.getPassword() == null) {
				ps.setString(2, edit.getAccount());
				ps.setString(3, edit.getName());
				ps.setInt(4, edit.getDepartment_id());
				ps.setInt(5, edit.getBranch_id());
				ps.setInt(6, edit.getId());
			}else {
				ps.setString(2, edit.getPassword());
				ps.setString(3, edit.getAccount());
				ps.setString(4, edit.getName());
				ps.setInt(5, edit.getDepartment_id());
				ps.setInt(6, edit.getBranch_id());
				ps.setInt(7, edit.getId());
			}

			System.out.println(ps);
			ps.executeUpdate();
			int id = user.getId();
			User loginuser = toUserList(connection,id);

			return loginuser;

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}

	private User toUserList(Connection connection,int id) throws SQLException {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ? ";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if (userList.isEmpty() == true) {
				return null;
			} else if (2 <= userList.size()) {
				throw new IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String password = rs.getString("password");
				String account = rs.getString("account");
				String name = rs.getString("name");
				int department_id = rs.getInt("department_id");
				int branch_id = rs.getInt("branch_id");
				int isstoped = rs.getInt("isstoped");
				Timestamp created_date = rs.getTimestamp("created_date");
				Timestamp updated_date = rs.getTimestamp("updated_date");

				User user = new User();
				user.setId(id);
				user.setPassword(password);
				user.setAccount(account);
				user.setName(name);
				user.setDepartment_id(department_id);
				user.setBranch_id(branch_id);
				user.setIsstoped(isstoped);
				user.setCreatedDate(created_date);
				user.setUpdatedDate(updated_date);

				ret.add(user);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public List<Department> Department(Connection connection) {
		// TODO 自動生成されたメソッド・スタブ
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("id, ");
			sql.append("name ");
			sql.append("FROM departments ");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<Department> ret = EditList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public List<Branch> Branch(Connection connection) {
		// TODO 自動生成されたメソッド・スタブ
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("id, ");
			sql.append("name ");
			sql.append("FROM branches ");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<Branch> ret = BranchList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Department> EditList(ResultSet rs)
			throws SQLException {

		List<Department> ret = new ArrayList<Department>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");

				Department departmant = new Department();
				departmant.setId(id);
				departmant.setName(name);

				ret.add(departmant);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	private List<Branch> BranchList(ResultSet rs)
			throws SQLException {

		List<Branch> ret = new ArrayList<Branch>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");

				Branch branch = new Branch();
				branch.setId(id);
				branch.setName(name);

				ret.add(branch);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
