package bulletin_board.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bulletin_board.service.MessageService;

/**
 * Servlet implementation class MessageDeleteServlet
 */
@WebServlet("/messagedelete")
public class MessageDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public MessageDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		//コメント削除処理
		if(isDelete(request) == true) {
		int id = Integer.parseInt(request.getParameter("message_id"));
		new MessageService().delete(id);
		response.sendRedirect("./");
		}else {
		response.sendRedirect("./");
		}
	}

		private boolean isDelete(HttpServletRequest request) {
			String id = request.getParameter("message_id");

			if(id.isEmpty()) {
				return false;
			}else {
				return true;
			}
	}
}
