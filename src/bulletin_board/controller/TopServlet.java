package bulletin_board.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulletin_board.beans.Comment;
import bulletin_board.beans.Message;
import bulletin_board.beans.User;
import bulletin_board.service.CommentService;
import bulletin_board.service.MessageService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		String start = request.getParameter("start");
		String end = request.getParameter("end");
		String param = request.getParameter("param");
		HttpSession session = request.getSession();


		User user = (User) request.getSession().getAttribute("loginUser");
		boolean isShowMessageForm;
		if (user != null) {
			isShowMessageForm = true;
		} else {
			isShowMessageForm = false;
		}

		Message message = new Message();
		List<String> errormsg = new ArrayList<String>();
		if(isValid(request, errormsg,start,end)) {
			if(!(StringUtils.isEmpty(start))) {
				start = start + " 00:00:00";
			}if( !(StringUtils.isEmpty(end))) {
				end = end + " 23:59:59";
			}
			message.setStart(start);
			message.setEnd(end);
			message.setParam(param);
			List<Message> messages = new MessageService().getMessage(message);
			List<Comment> comments = new CommentService().getComment();

			request.setAttribute("messages", messages);
			request.setAttribute("comments", comments);
			request.setAttribute("isShowMessageForm", isShowMessageForm);

			if(!(StringUtils.isEmpty(start))) {
				start = message.getStart().substring(0, message.getStart().indexOf(" "));
			}if(!(StringUtils.isEmpty(end))){
				end = message.getEnd().substring(0, message.getEnd().indexOf(" "));
			}

			request.setAttribute("start", start);
			request.setAttribute("end", end);
			request.setAttribute("num", param);
			request.getRequestDispatcher("/top.jsp").forward(request, response);

		}else {
			session.setAttribute("errorMessages", errormsg);
			response.sendRedirect("./");
		}
	}

		private boolean isValid(HttpServletRequest request, List<String> errormsg,String start,String end) {

			String param = request.getParameter("param");

			if(!((StringUtils.isBlank(start)|| StringUtils.isBlank(end)))) {
				try {
					start = start + " 00:00:00";
					end = end + " 23:59:59";

					DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					format.setLenient(false);
					format.parse(start);
					format.parse(end);
				}catch(ParseException e){
					errormsg.add("「開始日」「終了日」は「yyyy-mm-dd」形式で入力してください");
					return false;
				}
			}


			if(param != null) {
				if (10 < param.length()) {
					errormsg.add("カテゴリーは10文字以下で入力してください");
				}
			}

			if (errormsg.size() == 0) {
				return true;
			}else {
				return false;
			}
		}
}