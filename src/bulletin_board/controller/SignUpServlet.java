package bulletin_board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulletin_board.beans.User;
import bulletin_board.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<User> department = new UserService().getDepartment();
		List<User> branch = new UserService().getBranch();

		request.setAttribute("department", department);
		request.setAttribute("branch", branch );
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		List<String> accountList = new ArrayList<String>();
		accountList = new UserService().accountSelect();
		HttpSession session = request.getSession();

		if (isValid(request, messages, accountList) == true) {

			User user = new User();
			user.setPassword(request.getParameter("password"));
			user.setAccount(request.getParameter("accountname"));
			user.setName(request.getParameter("name"));
			user.setDepartment_id(Integer.parseInt(request.getParameter("department")));
			user.setBranch_id(Integer.parseInt(request.getParameter("branch")));
			new UserService().register(user);

			response.sendRedirect("./management");
		} else {
			String name = request.getParameter("name");
			String accountname = request.getParameter("accountname");
			int department_id = Integer.parseInt(request.getParameter("department"));
			int branch_id = Integer.parseInt(request.getParameter("branch"));
			List<User> department = new UserService().getDepartment();
			List<User> branch = new UserService().getBranch();

			session.setAttribute("errorMessages", messages);
			request.setAttribute("accountname",accountname);
			request.setAttribute("name", name);
			request.setAttribute("department_id",department_id);
			request.setAttribute("branch_id", branch_id);
			request.setAttribute("department", department);
			request.setAttribute("branch", branch );
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages, List<String> accountList) {
		String account = request.getParameter("accountname");
		String password = request.getParameter("password");
		String pass_ck = request.getParameter("pass_ck");
		String name = request.getParameter("name");
		String department = request.getParameter("department");
		String branch = request.getParameter("branch");

		//ログインIDチェック（半角英数字[azAZ0*9]で6文字以上20文字以下とします。）
		//パスワードチェック（記号を含む全ての半角文字で6文字以上20文字以下とします。）
		//名称チェック（ユーザーの名称です。個人名を格納します。名称は10文字以下とします。）

		//ログインID関連
		if (StringUtils.isBlank(account)) {
			messages.add("ログインIDを入力してください");
		}else if(!(account.matches("[0-9a-zA-Z]+")) || !(6 <= account.length() && account.length() <= 20)) {
			messages.add("ログインIDは半角英数字で6文字以上20文字以下とします");
		}

		for(String ac : accountList) {
			if(account.equals(ac)) {
				messages.add("ログインIDが重複しています");
			}
		}
		//パスワード関連
		if (!(password.equals(pass_ck))) {
			messages.add("パスワードが正しくありません");
		}
		if (StringUtils.isBlank(password)|| StringUtils.isBlank(pass_ck)) {
			messages.add("パスワードを入力してください");
		}else if(!(password.matches("[-_@+*;:#$%&0-9a-zA-Z]+")) || !(6 <= password.length() && password.length() <= 20)) {
			messages.add("パスワードは記号を含む全ての半角文字で6文字以上20文字以下とします");
		}
		//名称関連
		if(StringUtils.isBlank(name)) {
			messages.add("ユーザー名を入力してください");
		}else if(name.length()>10) {
			messages.add("ユーザー名は10文字までです");
		}
		//セレクトボックス確認
		if(branch.equals("1")) {
			if(!(department.equals("1") || department.equals("2"))) {
				messages.add("支店と部署・役職の組み合わせが不正です");
			}
		}else {
			if((department.equals("1") || department.equals("2"))){
				messages.add("支店と部署・役職の組み合わせが不正です");
			}
		}


		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}