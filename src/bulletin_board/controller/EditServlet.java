package bulletin_board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulletin_board.beans.Branch;
import bulletin_board.beans.Department;
import bulletin_board.beans.Management;
import bulletin_board.beans.User;
import bulletin_board.service.EditService;
import bulletin_board.service.ManagementService;

/**
 * Servlet implementation class EditServlet
 */
@WebServlet("/edit")
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EditServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if (isIdcheck(request, messages) == true) {
			int id = Integer.parseInt(request.getParameter("id"));

			User edit = new EditService().getEdit(id);
			List<Department> department = new EditService().getDepartment();
			List<Branch> branch = new EditService().getBranch();

			session.setAttribute("edit_id", id);
			session.setAttribute("account", edit.getAccount());
			request.setAttribute("edit", edit);
			request.setAttribute("department", department);
			request.setAttribute("branch", branch);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./management");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		//エラーメッセージの処理
		//登録処理
		List<String> messages = new ArrayList<String>();

		HttpSession session = request.getSession();
		if (isValid(request, messages) == true) {

			User edit = new User();
			edit.setId(Integer.parseInt(request.getParameter("id")));
			edit.setPassword(request.getParameter("password"));
			edit.setAccount(request.getParameter("account"));
			edit.setName(request.getParameter("name"));
			edit.setDepartment_id(Integer.parseInt(request.getParameter("department_id")));
			edit.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
			User user = (User) request.getSession().getAttribute("loginUser");
			User loginuser = new EditService().register(edit,user);
			if(!(loginuser == null)) {
				session.setAttribute("loginUser", loginuser);
			}
			response.sendRedirect("./management");
		} else {
			int id = (int) session.getAttribute("edit_id");

			User edit = new EditService().getEdit(id);
			List<Department> department = new EditService().getDepartment();
			List<Branch> branch = new EditService().getBranch();

			String name = request.getParameter("name");
			String account = request.getParameter("account");
			String password = request.getParameter("password");
			String pass_ck = request.getParameter("pass_ck");
			int branch_id = Integer.parseInt(request.getParameter("branch_id"));
			int department_id = Integer.parseInt(request.getParameter("department_id"));

			//値保持
			edit.setPassword(password);
			edit.setPass_ck(pass_ck);
			edit.setAccount(account);
			edit.setName(name);
			edit.setDepartment_id(department_id);
			edit.setBranch_id(branch_id);

			session.setAttribute("errorMessages", messages);
			request.setAttribute("id", id);
			request.setAttribute("edit", edit);
			request.setAttribute("department", department);
			request.setAttribute("branch", branch);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String name = request.getParameter("name");
		String account = request.getParameter("account");
		String password = request.getParameter("password");
		String pass_ck = request.getParameter("pass_ck");
		int branch = Integer.parseInt(request.getParameter("branch_id"));
		int department = Integer.parseInt(request.getParameter("department_id"));
		HttpSession session = request.getSession();
		String editaccount = (String) session.getAttribute("account");
		List<Management> management = new ManagementService().getManagement();

		if (StringUtils.isEmpty(account)) {
			messages.add("ログインIDを入力してください");
		}
		//既存ログインidチェック
				int count = 0;
				for (Management m : management) {
					if (m.getAccount().equals(account)) {
						if (!(editaccount.equals(account))) {
							count++;
						}
					}
				}
				//ログインID未入力
				if (!(StringUtils.isEmpty(account))) {
					//ログインID入力
					if (StringUtils.isBlank(account)) {
						messages.add("ログインIDにスペースが含まれています");
					}else if(!(account.matches("[0-9a-zA-Z]+")) || !(6 <= account.length() && account.length() <= 20)) {
						messages.add("ログインIDは半角英数字[azAZ0*9]で6文字以上20文字以下とします");
					}
				}
				if (count != 0) {
					messages.add("ログインIDが重複しています");
				}
		if (!(password.equals(pass_ck))) {
			messages.add("パスワード確認が正しくありません");
		}
		//パスワード未入力
		if (!(StringUtils.isEmpty(password)) || !(StringUtils.isEmpty(pass_ck))) {
			//パスワードスペース入力
			if (StringUtils.isBlank(password) || StringUtils.isBlank(pass_ck)) {
				messages.add("パスワードにスペースが含まれています");
			}else {
				if(!(password.matches("[-_@+*;:#$%&0-9a-zA-Z]+")) || !(6 <= password.length() && password.length() <= 20)) {
					messages.add("パスワードは記号を含む全ての半角文字で6文字以上20文字以下とします");
				}
				if(!(pass_ck.matches("[-_@+*;:#$%&0-9a-zA-Z]+")) || !(6 <= pass_ck.length() && pass_ck.length() <= 20)) {
					messages.add("パスワード(確認用)は記号を含む全ての半角文字で6文字以上20文字以下とします");
				}
			}
		}
		if (StringUtils.isEmpty(name)) {
			messages.add("ユーザー名を入力してください");
		}else if(!(name.length()<=10)) {
			messages.add("ユーザー名は10文字以下で入力してください");
		}
		if (branch != 1 && (department == 1 || department == 2)) {
			messages.add("支店と部署・役職の組み合わせが不正です");
		}
		if(branch == 1 && !(department == 1 || department == 2)) {
			messages.add("支店と部署・役職の組み合わせが不正です");
		}


		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

	private boolean isIdcheck(HttpServletRequest request, List<String> messages) {
		String id = request.getParameter("id");
		List<Management> management = new ManagementService().getManagement();

		//数値チェック
		if (!(StringUtils.isBlank(id))) {
			if (!(id.matches("^[0-9]+$"))) {
				messages.add("IDに不正な値が入力されました");
			}else if(id.length() >10){
				messages.add("IDに不正な値が入力されました");
			}else {
				//既存idチェック
				int count = 0;
				for (Management m : management) {
					if (m.getId() == Integer.parseInt(id)) {
						count++;
					}
				}
				if (count == 0) {
					messages.add("IDに不正な値が入力されました");
				}
			}
		}else {
			messages.add("IDに不正な値が入力されました");
		}
		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}
