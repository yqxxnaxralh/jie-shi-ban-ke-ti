package bulletin_board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulletin_board.beans.Comment;
import bulletin_board.beans.User;
import bulletin_board.service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> errormsg = new ArrayList<String>();

		HttpSession session = request.getSession();
		if (isValid(request, errormsg) == true) {

			User user = (User) session.getAttribute("loginUser");

			Comment comment = new Comment();
			comment.setUser_id(user.getId());
			comment.setMessage_id(Integer.parseInt(request.getParameter("message_id")));
			comment.setText(request.getParameter("comment"));
			new CommentService().register(comment);

			response.sendRedirect("./");
		}else {
			session.setAttribute("errorMessages", errormsg);
			response.sendRedirect("./");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> errormsg) {
		String comment = request.getParameter("comment");

		if (StringUtils.isBlank(comment)) {
			errormsg.add("コメントを入力してください");
		}if (500 <comment.length()) {
			errormsg.add("コメントは500字以下で入力してください");
		}if (errormsg.size() == 0) {
			return true;
		}else {
			return false;
		}
	}
}


