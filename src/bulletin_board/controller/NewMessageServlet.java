package bulletin_board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bulletin_board.beans.Message;
import bulletin_board.beans.User;
import bulletin_board.service.MessageService;



@WebServlet(urlPatterns = { "/newMessage" })
public class NewMessageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {
		request.getRequestDispatcher("newmessage.jsp").forward(request,response);

    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            User user = (User) session.getAttribute("loginUser");

            Message message = new Message();

            message.setUser_id(user.getId());
            message.setTitle(request.getParameter("subject"));
            message.setText(request.getParameter("text"));
            message.setCategory(request.getParameter("category"));

            new MessageService().register(message);

            response.sendRedirect("./");
        } else {
            String message = request.getParameter("text");
            String subject = request.getParameter("subject");
            String category = request.getParameter("category");

            session.setAttribute("errorMessages", messages);
            request.setAttribute("message", message);
            request.setAttribute("subject", subject);
			request.setAttribute("category", category);
			request.getRequestDispatcher("/newmessage.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String message = request.getParameter("text");
        String subject = request.getParameter("subject");
        String category = request.getParameter("category");

        //件名
        if(StringUtils.isBlank(subject)) {
        	messages.add("件名を入力してください");
        }
        if (30 < subject.length()) {
            messages.add("件名は30文字以下で入力してください");
        }
        //カテゴリー
        if(StringUtils.isBlank(category)) {
        	messages.add("カテゴリーを入力してください");
        }
        if (10 < category.length()) {
            messages.add("カテゴリーは10文字以下で入力してください");
        }
        //メッセージ
        if (StringUtils.isBlank(message) == true) {
            messages.add("メッセージを入力してください");
        }
        if (1000 < message.length()) {
            messages.add("メッセージは1000文字以下で入力してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}