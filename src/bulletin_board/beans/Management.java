package bulletin_board.beans;

import java.sql.Timestamp;

public class Management {
	private int id;
	private String account;
	private String name;
	private int department_id;
	private int branch_id;
	private int isstoped;
	private Timestamp updatedDate;
	private Timestamp createdDate;
	private String department;
	private String branch;


	public int getId() {
		// TODO 自動生成されたメソッド・スタブ
		return this.id;
	}
	public void setId(int id) {
		// TODO 自動生成されたメソッド・スタブ
		this.id = id;
	}

	public String getName() {
		// TODO 自動生成されたメソッド・スタブ
		return this.name;
	}
	public void setName(String name) {
		// TODO 自動生成されたメソッド・スタブ
		this.name = name;
	}

	public String getAccount() {
		// TODO 自動生成されたメソッド・スタブ
		return this.account;
	}
	public void setAccount(String account) {
		// TODO 自動生成されたメソッド・スタブ
		this.account = account;
	}

	public int getDepartment_id() {
		// TODO 自動生成されたメソッド・スタブ
		return this.department_id;
	}
	public void setDepartment_id(int department_id) {
		// TODO 自動生成されたメソッド・スタブ
		this.department_id = department_id;
	}

	public int getBranch_id() {
		// TODO 自動生成されたメソッド・スタブ
		return this.branch_id;
	}
	public void setBranch_id(int branch_id) {
		// TODO 自動生成されたメソッド・スタブ
		this.branch_id = branch_id;
	}


	public int getIsstoped() {
		// TODO 自動生成されたメソッド・スタブ
		return this.isstoped;
	}
	public void setIsstoped(int isstoped) {
		// TODO 自動生成されたメソッド・スタブ
		this.isstoped = isstoped;
	}


	public Timestamp getCreatedDate() {
		// TODO 自動生成されたメソッド・スタブ
		return this.createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		// TODO 自動生成されたメソッド・スタブ
		this.createdDate = createdDate;
	}


	public Timestamp getUpdatedDate() {
		// TODO 自動生成されたメソッド・スタブ
		return this.updatedDate;
	}
	public void setUpdatedDate(Timestamp updatedDate) {
		// TODO 自動生成されたメソッド・スタブ
		this.updatedDate = updatedDate;
	}
	public String getDepartment() {
		return this.department;
	}
	public void setDepartment(String department) {
		// TODO 自動生成されたメソッド・スタブ
		this.department = department;
	}
	public String getBranch() {
		return this.branch;
	}
	public void setBranch(String branch) {
		// TODO 自動生成されたメソッド・スタブ
		this.branch = branch;
	}

}
