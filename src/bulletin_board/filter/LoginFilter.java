package bulletin_board.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class LoginFilter
 */
@WebFilter(urlPatterns = { "/*" })
public class LoginFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public LoginFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here

		List<String> errormsg = new ArrayList<String>();
		System.out.println("ログインチェック");
		String target = ((HttpServletRequest)request).getRequestURI();

		// セッションが存在しない場合NULLを返す
		HttpSession session = ((HttpServletRequest) request).getSession(false);

		if (session == null) {
			if(target.equals("/osuga_kensuke/css/style.css")) {
				chain.doFilter(request, response);
			}else {
				session = ((HttpServletRequest)request).getSession(true);
		        ((HttpServletResponse)response).sendRedirect("./login");
			}
		}else {
			Object user = session.getAttribute("loginUser");
			if (user == null) {
				if(target.equals("/osuga_kensuke/css/style.css")) {
					chain.doFilter(request, response);
					return;
				}
                // ログインユーザーがNullならば、ログイン画面へ飛ばす
				if(target.equals("/osuga_kensuke/login")) {
					chain.doFilter(request, response);
				}else {
		        	errormsg.add("ログインしてください");
		        	request.setAttribute("errorMessages", errormsg);
			        ((HttpServletResponse)response).sendRedirect("./login");
				}
			}else {
				// ログインユーザーがNULLでなければ、通常どおりの遷移
				chain.doFilter(request, response);
			}
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig config) throws ServletException {
	}

	/**
	 * @see Filter#destroy()
	 * インスタンスの破棄
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

}