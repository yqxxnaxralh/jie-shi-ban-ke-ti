package bulletin_board.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bulletin_board.beans.User;

/**
 * Servlet Filter implementation class AuthorityFilter
 */
@WebFilter(urlPatterns = { "/management", "/edit", "/signup" })
public class AuthorityFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public AuthorityFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	@SuppressWarnings("unused")
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here

		// pass the request along the filter chain

		List<String> messages = new ArrayList<String>();
		HttpServletRequest httpRequest = (HttpServletRequest)request;
		HttpServletResponse httpResponse = (HttpServletResponse)response;
		HttpSession session = ((HttpServletRequest) request).getSession();
		User user = (User) session.getAttribute("loginUser");
		String target = ((HttpServletRequest) request).getRequestURI();


		//ユーザーが存在するか
		if (user != null) {
			int user_department = user.getDepartment_id();
			int user_branch = user.getBranch_id();
			if(!(target.equals("/osuga_kensuke/management") || target.equals("/osuga_kensuke/signup") || target.equals("/osuga_kensuke/edit"))){
	        	((HttpServletResponse)response).sendRedirect("./");
			}
			//権限を持っているか
			else if (user_department == 1 && user_branch == 1) {
				System.out.println("権限を確認しました");
				chain.doFilter(request, response);
			} else {
				messages.add("アクセス権限がありません");
				session.setAttribute("errorMessages", messages);
				((HttpServletResponse)response).sendRedirect("./");
					}
		} else {
        	messages.add("ログインしてください");
        	session.setAttribute("errorMessages", messages);
        	((HttpServletResponse)response).sendRedirect("./login");
		}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

}
