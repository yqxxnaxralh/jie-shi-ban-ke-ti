package bulletin_board.service;

import static bulletin_board.util.CloseableUtil.*;
import static bulletin_board.util.DBUtil.*;

import java.sql.Connection;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import bulletin_board.beans.Message;
import bulletin_board.dao.MessageDao;

public class MessageService {

	public void register(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.insert(connection, message);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	private static final int LIMIT_NUM = 1000;

	public List<Message> getMessage(Message message) {

        Connection connection = null;
        List<Message> ret = new ArrayList<Message>();
        try {
            connection = getConnection();
            try {
				Timestamp st = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2015-01-01 00:00:00").getTime());
				Timestamp ed = new Timestamp(System.currentTimeMillis());


				//開始日、終了日null
	            if(message.getEnd() == null && message.getStart() == null || message.getStart().isEmpty() && message.getEnd().isEmpty()) {
	            	message.setStartDate(st);
	            	message.setEndDate(ed);
					if(message.getParam() == null || message.getParam() == "") {
						message.setParam("%");
					}else {
						String param = message.getParam().trim();
						message.setParam("%"+param+"%");
					}
	            }else{
	            	//開始日のみnull
	            	if(message.getStart() == null || message.getStart().isEmpty()) {
		            	message.setStartDate(st);
		            	Timestamp end = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(message.getEnd()).getTime());
		            	//デフォルト比較
		            	if(end.after(ed)) {
		            		message.setEndDate(ed);
		            	}else {
		            		message.setEndDate(end);
		            	}
		            	if(message.getParam() == null || message.getParam().isEmpty()) {
							message.setParam("%");
						}else {
							String param = message.getParam().trim();
							message.setParam("%"+param+"%");
						}
		            //終了日のみnull
		            }else if(message.getEnd().isEmpty()) {
		            	message.setEndDate(ed);

		            	Timestamp start = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(message.getStart()).getTime());
		            	if(start.before(st)) {
		            		message.setStartDate(st);
		            	}else {
			            	message.setStartDate(start);
		            	}
						if(StringUtils.isBlank(message.getParam())) {
							message.setParam("%");
						}else {
							String param = message.getParam().trim();
							message.setParam("%"+param+"%");
						}
	            	}else {
	            		//開始日、終了日入力処理
	            		Timestamp start = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(message.getStart()).getTime());
	            		Timestamp end = new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(message.getEnd()).getTime());

		            	if(start.before(st)) {
		            		message.setStartDate(st);
		            	}else {
			            	message.setStartDate(start);
		            	}

		            	if(end.after(ed)) {
		            		message.setEndDate(ed);
		            	}else {
		            		message.setEndDate(end);
		            	}

						if(message.getParam() == null || message.getParam().isEmpty()) {
							message.setParam("%");
						}else {
							String param = message.getParam().trim();
							message.setParam("%"+param+"%");
						}
		            }
	            }
	            MessageDao messageDao = new MessageDao();
	            ret = messageDao.getUserMessages(connection, message, LIMIT_NUM);

	            return ret;
			} catch (ParseException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
    		return ret;

        }catch(RuntimeException e){
		rollback(connection);
		throw e;
	}catch(Error e){
		rollback(connection);
		throw e;
	}finally{
		close(connection);
	}
	}

	public void delete(int id) {
		// TODO 自動生成されたメソッド・スタブ
		Connection connection = null;
		try {
			connection = getConnection();

			MessageDao messageDao = new MessageDao();
			messageDao.delete(connection, id);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}