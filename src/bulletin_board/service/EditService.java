package bulletin_board.service;

import static bulletin_board.util.CloseableUtil.*;
import static bulletin_board.util.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import bulletin_board.beans.Branch;
import bulletin_board.beans.Department;
import bulletin_board.beans.User;
import bulletin_board.dao.EditDao;
import bulletin_board.util.CipherUtil;

public class EditService {

	public User getEdit(int id) {
		// TODO 自動生成されたメソッド・スタブ
        Connection connection = null;
        try {
            connection = getConnection();

            EditDao editDao = new EditDao();
            User edit = editDao.getEditDao(connection, id);

    		return edit;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public User register(User edit ,User user) {
		// TODO 自動生成されたメソッド・スタブ

		Connection connection = null;
		User loginuser;
		try {
			connection = getConnection();
			EditDao editDao = new EditDao();
			if(!(StringUtils.isEmpty(edit.getPassword()))) {
				String encPassword = CipherUtil.encrypt(edit.getPassword());
				edit.setPassword(encPassword);
				loginuser = editDao.Update(connection, edit,user);
			}else {
				String encPassword = null;
				edit.setPassword(encPassword);
				loginuser = editDao.Update(connection, edit,user);
			}
			commit(connection);

			return loginuser;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<Department> getDepartment() {
		// TODO 自動生成されたメソッド・スタブ
		Connection connection = null;
		try {
			connection = getConnection();
			EditDao editDao = new EditDao();
			List<Department> ret = editDao.Department(connection);

			return ret;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<Branch> getBranch() {
		// TODO 自動生成されたメソッド・スタブ
		Connection connection = null;
		try {
			connection = getConnection();
			EditDao editDao = new EditDao();
			List<Branch> ret = editDao.Branch(connection);

			return ret;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
