package bulletin_board.service;

import static bulletin_board.util.CloseableUtil.*;
import static bulletin_board.util.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bulletin_board.beans.User;
import bulletin_board.dao.UserDao;
import bulletin_board.util.CipherUtil;

public class UserService {

	public void register(User user) {
		// TODO 自動生成されたメソッド・スタブ

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<String> accountSelect() {
		// TODO 自動生成されたメソッド・スタブ
		Connection connection = null;
		try {
			connection = getConnection();

			UserDao userDao = new UserDao();
			List<String> account = userDao.getAccount(connection);

			commit(connection);

			return account;
		}catch(RuntimeException e) {
			rollback(connection);
			throw e;
		}catch(Error e) {
			rollback(connection);
			throw e;
		}finally {
			close(connection);
		}
	}

	public List<User> getDepartment() {
		// TODO 自動生成されたメソッド・スタブ
		Connection connection = null;
		try {
			connection = getConnection();
			UserDao userDao = new UserDao();
			List<User> ret = userDao.Department(connection);

			return ret;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<User> getBranch() {
		// TODO 自動生成されたメソッド・スタブ
		Connection connection = null;
		try {
			connection = getConnection();
			UserDao UserDao = new UserDao();
			List<User> ret = UserDao.Branch(connection);

			return ret;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
